package com.eisenhower.my_beautiful_news;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;

import com.crashlytics.android.Crashlytics;
import com.eisenhower.my_beautiful_news.Connection.CheckPermission;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONObject;

import io.fabric.sdk.android.Fabric;

public class MainActivity extends AppCompatActivity implements  ArticleListFragment.OnHeadlineSelectedListener{

JSONObject jsonObject ;
    private PendingIntent pendingIntent;
    MyReceiver myReceiver = new MyReceiver();

    private static final String TAG = "MainActivity";
    private boolean internetConnected=true;
    private Snackbar snackbar;
    ArticleListFragment firstFragment;
    SharedPreferences preferences;
    boolean canRemind=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if(getResources().getBoolean(R.bool.portrait_only)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        preferences = PreferenceManager.getDefaultSharedPreferences(this);

      //  Fabric.with(this, new Crashlytics());

        final Fabric fabric = new Fabric.Builder(this)
                .kits(new Crashlytics())
                .debuggable(true)
                .build();
        Fabric.with(fabric);
//        forceCrash();
        canRemind = preferences.getBoolean("canRemind",true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setSharedElementExitTransition(null);
        }
        Bundle bundle = getIntent().getExtras();
        if(bundle!=null){
            if(bundle.containsKey("json")) {
                String jsonString = bundle.getString("json");
                try {
                    if(jsonString.length()>10)
                    jsonObject = new JSONObject(jsonString);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
//         Log.i("loadData1",jsonObject.toString());

        }



        // Create an instance of ExampleFragment
  firstFragment = new ArticleListFragment();

        // In case this activity was started with special instructions from an Intent,
        // pass the Intent's extras to the fragment as arguments
        firstFragment.setArguments(getIntent().getExtras());
        firstFragment.setArguments(getIntent().getBundleExtra("json1"));
//firstFragment.setExitSharedElementCallback(null);
        // Add the fragment to the 'fragment_container' FrameLayout


        getSupportFragmentManager().beginTransaction()
                .add(R.id.listFragment, firstFragment,"list").commit();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .build();
        ImageLoader.getInstance().init(config);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            getWindow().setAllowEnterTransitionOverlap(false);
        }

        ((SearchView)findViewById(R.id.tot_search)).setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                hideSearch();

                return false;
            }
        });
createNotification();
      /*  Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, 16);
      //  calendar.set(Calendar.MONTH, 4);
        //calendar.set(Calendar.YEAR, 2017);
        //calendar.set(Calendar.DAY_OF_MONTH, 17);

        calendar.set(Calendar.HOUR_OF_DAY, 16);
        calendar.set(Calendar.MINUTE, 00);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.AM_PM,Calendar.PM);
        Log.i("date",calendar.getTime().toGMTString());
        Log.i("date",calendar.getTime().getMonth()+"");
        Intent myIntent = new Intent(MainActivity.this, MyReceiver.class);
        pendingIntent = PendingIntent.getBroadcast(MainActivity.this, 0, myIntent,0);

    alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                AlarmManager.INTERVAL_DAY, pendingIntent);*/
      //  alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),1000*60*2, pendingIntent);
     //   alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),AlarmManager.INTERVAL_DAY, pendingIntent);
      //  alarmManager.set(AlarmManager.RTC, calendar.getTimeInMillis(), pendingIntent);


        CheckPermission.verifyStoragePermissions(this);
        if(canRemind) {
            myReceiver.setAlarm(this);
        }else{
            myReceiver.cancelAlarm(this);
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }





    @Override
    public void onBackPressed() {
        Log.i("sear","we are hrer0 "+ getSupportFragmentManager().getBackStackEntryCount());
        Log.i("sear","we are hrer0 "+findViewById(R.id.tot_search).isShown());
//SearchFragment searchFragment= (SearchFragment) getSupportFragmentManager().findFragmentByTag("mySearch");
        if(getSupportFragmentManager().getBackStackEntryCount()>0) {
            if (findViewById(R.id.tot_search).isShown()) {
                hideSearch();


            }
            super.onBackPressed();
        }else{
           finish();
        }






       // }
     //   if(getFragmentManager().)
    /*  if (getFragmentManager().getBackStackEntryCount() > 0 ){
            Log.i("sear","we are hrer");
            if(findViewById(R.id.tot_search).isActivated())
            {
                Log.i("sear","we are hrer2");

                hideSearch();
            }
          getFragmentManager().popBackStack();
        } else {


     //   findViewById(R.id.tot_search).setVisibility(View.INVISIBLE);
       // imageView.setVisibility(View.GONE);
      ///  searchView.setVisibility(View.VISIBLE);
      //  searchView.setIconified(false);
          finish();
           super.onBackPressed();

        }*/

    }


    public  void hideSearch(){
        if(  ((SearchView)findViewById(R.id.tot_search))!=null) {
            ((SearchView) findViewById(R.id.tot_search)).setIconified(true);
            ((SearchView) findViewById(R.id.tot_search)).setVisibility(View.GONE);
            findViewById(R.id.top_logo).setVisibility(View.VISIBLE);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();

    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
         //   startActivity(new Intent(MainActivity.this, SettingsActivity.class));

            SearchFragment searchFragment = new SearchFragment();
            searchFragment.setArguments(getIntent().getExtras());
            searchFragment.setArguments(getIntent().getBundleExtra("json1"));

            // Add the fragment to the 'fragment_container' FrameLayout
            final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.listFragment, searchFragment, "mySearch");
            transaction.addToBackStack(null);
            transaction.commit();
          //  getSupportFragmentManager().beginTransaction()
//.addToBackStack("tag")
         //           .add(R.id.listFragment, searchFragment).commit();

            return true;
        }
        else if(id==R.id.action_share_story){
            ShareStoryFragment shareFragment = new  ShareStoryFragment();

            final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.listFragment, shareFragment, "myShare");
            transaction.addToBackStack(null);
            transaction.commit();
          //  startActivity(new Intent(MainActivity.this,ShareStory.class));
        }else if(id ==R.id.action_settings){
          SettingsFragment settingsFragment = new      SettingsFragment();

            final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.listFragment, settingsFragment, "mySetting");
            transaction.addToBackStack(null);
            transaction.commit();
        }else if(id==R.id.action_about){

           /// Intent intent = new Intent(MainActivity.this,MyWebView.class);
            Uri uriUrl = Uri.parse("http://beautifulnews.news24.com/about?csref=BeautifulNews_ClickTracker_OMG_AboutBN_Q2-2017");

            Intent shareIntent = new Intent(Intent.ACTION_VIEW,uriUrl);

            startActivity(shareIntent);

            //intent.putExtra("url","http://beautifulnews.news24.com/about?csref=BeautifulNews_ClickTracker_OMG_AboutBN_Q2-2017");
           // startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public Bundle getBundle() {
        return getIntent().getExtras();


    }


    public void createNotification() {

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        //We get a reference to the NotificationManager
      /*  NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        String MyText = "Reminder";
        Notification mNotification = new Notification(R.mipmap.ic_launcher, MyText, System.currentTimeMillis() );
        //The three parameters are: 1. an icon, 2. a title, 3. time when the notification appears

        String MyNotificationTitle = "Medicine!";
        String MyNotificationText  = "Don't forget to take your medicine!";

        Intent MyIntent = new Intent(Intent.ACTION_VIEW);
        PendingIntent StartIntent = PendingIntent.getActivity(getApplicationContext(),0,MyIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        //A PendingIntent will be fired when the notification is clicked. The FLAG_CANCEL_CURRENT flag cancels the pendingintent

        mNotification.setLatestEventInfo(getApplicationContext(), MyNotificationTitle, MyNotificationText, StartIntent);


        notificationManager.notify(NOTIFICATION_ID , mNotification);
        //We are passing the notification to the NotificationManager with a unique id.*/


    }


    public void forceCrash() {
        throw new RuntimeException("This is a crash");
    }

}
