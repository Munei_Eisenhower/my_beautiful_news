package com.eisenhower.my_beautiful_news;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.eisenhower.my_beautiful_news.Model.Article;
import com.google.android.gms.ads.NativeExpressAdView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * The {@link RecyclerViewAdapter} class.
 * <p>The adapter provides access to the items in the {@link MenuItemViewHolder}
 * or the {@link NativeExpressAdViewHolder}.</p>
 */
class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    // A menu item view type.
    private static final int MENU_ITEM_VIEW_TYPE = 0;

    // The Native Express ad view type.
    private static final int NATIVE_EXPRESS_AD_VIEW_TYPE = 1;

    // An Activity's Context.
    private final Context mContext;
    DisplayImageOptions options;
    ImageLoader imageLoader;
    Context context;
    // The list of Native Express ads and menu items.
    private  List<Object> mRecyclerViewItems;

    /**
     * For this example app, the recyclerViewItems list contains only
     * {@link com.eisenhower.my_beautiful_news.Model.Article} and {@link NativeExpressAdView} types.
     */
    public RecyclerViewAdapter(Context context, List<Object> recyclerViewItems) {
        this.mContext = context;
        this.context = context;
        this.mRecyclerViewItems = recyclerViewItems;

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.loading) // resource or drawable
                .showImageForEmptyUri(R.drawable.img_not_available) // resource or drawable
                .showImageOnFail(R.drawable.img_not_available) // resource or drawable
                .delayBeforeLoading(1000)
                .resetViewBeforeLoading(true)  // default
                .cacheInMemory(true) // default => false
                .cacheOnDisk(true) // default => false
                .build();
        imageLoader = ImageLoader.getInstance();
        try {
            count();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public  void addAds(List<Object> objectList){
        mRecyclerViewItems=objectList;
        notifyDataSetChanged();
    }
    /**
     * The {@link MenuItemViewHolder} class.
     * Provides a reference to each view in the menu item view.
     */
    public class MenuItemViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener {
        public TextView title,views,date,textViewOffline;
        public ImageView coverPicture;

        MenuItemViewHolder(View view) {
            super(view);
            title = (TextView) itemView.findViewById(R.id.my_text);
            views = (TextView) itemView.findViewById(R.id.my_text_views);
            date = (TextView) itemView.findViewById(R.id.my_text_date);
            textViewOffline = (TextView) itemView.findViewById(R.id.isSavedOffline);
            coverPicture = (ImageView) itemView.findViewById(R.id.cover_picture);
            itemView.setOnClickListener(this);
        }
        @Override
        public void onClick(View v) {
            if(!mRecyclerViewItems.isEmpty()) {
                Article article = (Article) mRecyclerViewItems.get(getAdapterPosition());

                Intent intent = new Intent(context, ArticleDetailsFull.class);
                intent.putExtra("position", getAdapterPosition());
                intent.putExtra("articleId", article.getArticleId());
                intent.putExtra("articleTitle", article.getTitle());
                intent.putExtra("articleDate", article.getDisplayDate());
                intent.putExtra("articleSmallThumbnailUrl", article.getSmallThumbnailUrl());
                intent.putExtra("articleMediumThumbnailUrl", article.getMediumThumbnailUrl());
                intent.putExtra("articleLargeThumbnailUrl", article.getLargeThumbnailUrl());
                intent.putExtra("articleUrl", article.getArticleUrl());
                intent.putExtra("isSaved", article.isSaved());
                if (article.isSaved()) {
                    intent.putExtra("articleVideoPath", article.getVideoLocalPath());
                    intent.putExtra("articleVideoUrl", article.getVideoUrl());
                    intent.putExtra("articleViews", article.getArticleViews());
                    intent.putExtra("articleBody", article.getArticleBody());
                }

                context.startActivity(intent);
            }

        }
    }

    /**
     * The {@link NativeExpressAdViewHolder} class.
     */
    public class NativeExpressAdViewHolder extends RecyclerView.ViewHolder {

        NativeExpressAdViewHolder(View view) {
            super(view);
        }
    }

    @Override
    public int getItemCount() {
        return mRecyclerViewItems.size();
    }

    /**
     * Determines the view type for the given position.
     */
    @Override
    public int getItemViewType(int position){

        if(mRecyclerViewItems.get(position) instanceof Article){
            return MENU_ITEM_VIEW_TYPE;
        }else{
            return NATIVE_EXPRESS_AD_VIEW_TYPE;
        }




   /*     return (position % ArticleListFragment.ITEMS_PER_AD == 0) ? NATIVE_EXPRESS_AD_VIEW_TYPE
                : MENU_ITEM_VIEW_TYPE;*/
    }

    public void adAds(List<Object> objectList){
        mRecyclerViewItems = objectList;
        notifyDataSetChanged();

    }

    /**
     * Creates a new view for a menu item view or a Native Express ad view
     * based on the viewType. This method is invoked by the layout manager.
     */
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        switch (viewType) {
            case MENU_ITEM_VIEW_TYPE:
                View menuItemLayoutView = LayoutInflater.from(viewGroup.getContext()).inflate(
                        R.layout.list_item_card, viewGroup, false);
                return new MenuItemViewHolder(menuItemLayoutView);
            case NATIVE_EXPRESS_AD_VIEW_TYPE:
                // fall through
            default:
                View nativeExpressLayoutView = LayoutInflater.from(
                        viewGroup.getContext()).inflate(R.layout.native_express_ad_container,
                        viewGroup, false);
                return new NativeExpressAdViewHolder(nativeExpressLayoutView);
        }

    }

    /**
     *  Replaces the content in the views that make up the menu item view and the
     *  Native Express ad view. This method is invoked by the layout manager.
     */
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int viewType = getItemViewType(position);
        switch (viewType) {
            case MENU_ITEM_VIEW_TYPE:
                MenuItemViewHolder menuItemHolder = (MenuItemViewHolder) holder;
                Article article = (Article) mRecyclerViewItems.get(position);

                // Get the menu item image resource ID.
if(article.getArticleViews()==0){
    menuItemHolder.views.setVisibility(View.INVISIBLE);

}else{
    menuItemHolder.views.setVisibility(View.VISIBLE);
}
                // Add the menu item details to the menu item view.
                if(article.isSaved()){
                    imageLoader.denyNetworkDownloads(true);
                   // menuItemHolder.views.setVisibility(View.VISIBLE);
                    menuItemHolder.textViewOffline.setVisibility(View.VISIBLE);

                }else{
                    imageLoader.denyNetworkDownloads(false);
                 //   menuItemHolder.views.setVisibility(View.INVISIBLE);
                    menuItemHolder.textViewOffline.setVisibility(View.INVISIBLE);
                }
                menuItemHolder.title.setText(article.getTitle());

                menuItemHolder.views.setText(article.getArticleViews()+" views");
                menuItemHolder.date.setText(article.getDisplayDate());
                Log.i("p1",article.isSaved()+" ");
                Log.i("p1",article.getVideoLocalPath()+" ");
                Log.i("p1",article.getSmallThumbnailUrl());


                if(context. getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                    imageLoader.displayImage(article.getMediumThumbnailUrl(), menuItemHolder.coverPicture, options, null);
                }else{

                    imageLoader.displayImage(article.getSmallThumbnailUrl(),menuItemHolder.coverPicture, options, null);


                }
                break;
            case NATIVE_EXPRESS_AD_VIEW_TYPE:
                // fall through
            default:
                NativeExpressAdViewHolder nativeExpressHolder =
                        (NativeExpressAdViewHolder) holder;
                NativeExpressAdView adView =
                        (NativeExpressAdView) mRecyclerViewItems.get(position);

                ViewGroup adCardView = (ViewGroup) nativeExpressHolder.itemView;
                // The NativeExpressAdViewHolder recycled by the RecyclerView may be a different
                // instance than the one used previously for this position. Clear the
                // NativeExpressAdViewHolder of any subviews in case it has a different
                // AdView associated with it, and make sure the AdView for this position doesn't
                // already have a parent of a different recycled NativeExpressAdViewHolder.
                if (adCardView.getChildCount() > 0) {
                    adCardView.removeAllViews();
                }
                if (adView.getParent() != null) {
                    ((ViewGroup) adView.getParent()).removeView(adView);
                }

                // Add the Native Express ad to the native express ad view.
                adCardView.addView(adView);
        }
    }

public void count() throws Exception{
    long m=0,tue=0,w=0,t=0,f=0,sat=0,s=0;
    long mv=0,tuev=0,wv=0,tv=0,fv=0,satv=0,sv=0;
    for(Object article :mRecyclerViewItems){
        if(article instanceof Article) {
            Article article1 = (Article) article;
            SimpleDateFormat sdf = new SimpleDateFormat("d MMMM yyyy");
            Date date = sdf.parse(article1.getDisplayDate());
            sdf.format(date);
            Calendar calendar = sdf.getCalendar();
            if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY) {
                m++;
                mv += article1.getArticleViews();
                Log.i("Mydate1", "Monday:" + article1.getDisplayDate() + "  Total:" + m + " views:" + mv);
            } else if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY) {
                tue++;
                tuev += article1.getArticleViews();
                Log.i("Mydate2", "Tues:" + article1.getDisplayDate() + "  Total:" + tue + " views:" + tuev);
            } else if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY) {
                w++;
                wv += article1.getArticleViews();
                Log.i("Mydate3", "Wed:" + article1.getDisplayDate() + "  Total:" + w + " views:" + wv);
            } else if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY) {
                t++;
                tv += article1.getArticleViews();
                Log.i("Mydate4", "Thurs:" + article1.getDisplayDate() + "  Total:" + t + " views:" + tv);
            } else if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY) {
                f++;
                fv += article1.getArticleViews();
                Log.i("Mydate5", "Friday:" + article1.getDisplayDate() + "  Total:" + f + " views:" + fv);
            } else if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
                sat++;
                satv += article1.getArticleViews();
                Log.i("Mydate6", "Sat:" + article1.getDisplayDate() + "  Total:" + sat + " views:" + sat);
            } else if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
                s++;
                sv += article1.getArticleViews();
                Log.i("Mydate7", "Sun:" + article1.getDisplayDate() + "  Total:" + s + " views:" + sv);
            } else {
                Log.i("Mydate8", "nothing");
            }
        }
    }
  //  Log.i("M1","Monday total ads:"+m+" made "+mv+ " views"+" and Average views:"+(mv/m));
 //   Log.i("M1","Tues total ads:"+tue+" made "+tuev+ " views" +" and Average views:"+(tuev/tue));
 //   Log.i("M1","Wed total ads:"+w+" made "+wv+ " views"+" and Average views:"+(wv/w));
//    Log.i("M1","Thur total ads:"+t+" made "+tv+ " views"+" and Average views:"+(tv/t));
 //   Log.i("M1","Friday total ads:"+f+" made "+fv+ " views"+" and Average views:"+(fv/f));
 //   Log.i("M1","Sat total ads:"+sat+" made "+satv+ " views"+" and Average views:"+(satv/sat));
//    Log.i("M1","Sunday total ads:"+s+" made "+sv+ " views"+" and Average views:"+(sv/s));

}

}
