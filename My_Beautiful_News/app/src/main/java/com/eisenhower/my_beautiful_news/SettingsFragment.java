package com.eisenhower.my_beautiful_news;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.eisenhower.my_beautiful_news.Connection.DatabaseHelper;
import com.eisenhower.my_beautiful_news.Model.Article;

import java.io.File;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends Fragment {

TextView textViewNumber;
    Button buttonClear;
    DatabaseHelper databaseHelper;
    int countArticles=0;
     Switch aSwitchSave;
     Switch aSwitchReminder;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    public SettingsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for thi s fragment
        setHasOptionsMenu(true);
        databaseHelper = new DatabaseHelper(getActivity());
        countArticles = databaseHelper.getArticleCount();
        preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        editor = preferences.edit();
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        View  view = inflater.inflate(R.layout.fragment_settings, container, false);
        textViewNumber =(TextView)view.findViewById(R.id.textViewArticleNumber);

        aSwitchSave = (Switch)view.findViewById(R.id.switchSave);
        aSwitchReminder = (Switch)view.findViewById(R.id.switch_reminder);
        buttonClear =(Button)view.findViewById(R.id.buttonClear);
        textViewNumber.setText(countArticles+"");

       boolean saveVideo = preferences.getBoolean("saveVideo", false);
       boolean canRemind = preferences.getBoolean("canRemind", true);

         aSwitchSave.setChecked(saveVideo);
        aSwitchReminder.setChecked(canRemind);

        aSwitchReminder.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    editor.putBoolean("canRemind", true);
                }else{
                    editor.putBoolean("canRemind", false);
                }
                editor.commit();
            }
        });
        aSwitchSave.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    editor.putBoolean("saveVideo", true);
                }else{
                    editor.putBoolean("saveVideo", false);
                }
                editor.commit();
            }
        });


        buttonClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteDatabase(databaseHelper.getAllArticle());
            }
        });

        return view;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if(id==android.R.id.home){

            getActivity().onBackPressed();
        }


        return super.onOptionsItemSelected(item);
    }

    public void deleteDatabase(List<Article> articleList){
        File file =null;
        if(!articleList.isEmpty()) {
            for (Article articleDb : articleList) {
                file = new File(articleDb.getVideoLocalPath());
                if(file.exists()){
                    file.delete();
                }
            }
            getActivity().deleteDatabase(databaseHelper.getDatabaseName());
            textViewNumber.setText(databaseHelper.getArticleCount()+"");
        }
    }

}
