package com.eisenhower.my_beautiful_news;


import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.eisenhower.my_beautiful_news.Connection.DatabaseHelper;
import com.eisenhower.my_beautiful_news.Connection.JSONParser;
import com.eisenhower.my_beautiful_news.Connection.MyNetwork;
import com.eisenhower.my_beautiful_news.Model.Article;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.NativeExpressAdView;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
/**
 * A simple {@link Fragment} subclass.
 */
public class ArticleListFragment extends Fragment implements SearchView.OnQueryTextListener {


    private RecyclerView mRecyclerView;
    RecyclerViewAdapter adapter;
    private FirebaseRemoteConfig mFirebaseRemoteConfig;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<Article> articleList = new ArrayList<>();
    private List<Object> articleListWithAds = new ArrayList<>();
    DatabaseHelper databaseHelper;
    JSONObject jsonObjectDownloaded;
    Snackbar snackbar=null;
    private boolean isReceiverRegistered = false;
    OnHeadlineSelectedListener mCallback;
    NetworkChangeReceiver networkChangeReceiver;
    SwipeRefreshLayout mSwipeRefreshLayout;
    List<String> itemsAds=null;
    private AdView mAdView;
    View toCallView=null;
    String adsList="";

    // The container Activity must implement this interface so the frag can deliver messages
    public interface OnHeadlineSelectedListener {
        /** Called by HeadlinesFragment when a list item is selected */

        public Bundle getBundle();
    }
    public ArticleListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.bm_fragment, container, false);
        toCallView =view;
        mAdView = (AdView) view.findViewById(R.id.adView);
        networkChangeReceiver = new NetworkChangeReceiver();
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
           //     .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build();
        mFirebaseRemoteConfig.setConfigSettings(configSettings);
        mFirebaseRemoteConfig.setDefaults(R.xml.remote_config_defaults);


        if(mCallback.getBundle()==null){
            Log.i("loadData","null here");

        }else{
            try {
                if(mCallback.getBundle().containsKey("adsList")) {
                    adsList = mCallback.getBundle().getString("adsList");
                }
                if(mCallback.getBundle().containsKey("json")) {
                    if(mCallback.getBundle().getString("json").length()>10)
                    jsonObjectDownloaded = new JSONObject(mCallback.getBundle().getString("json"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
      itemsAds = Arrays.asList(adsList.split("\\s*,\\s*"));



        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);

         setUpRecylceView();

        setHasOptionsMenu(true);


       databaseHelper = new DatabaseHelper(getActivity());
        if(databaseHelper.getArticleCount()>0) {
            databaseHelper.deleteOldData();
        }

        if(jsonObjectDownloaded!=null) {
            readData(jsonObjectDownloaded);
        }
           else {

               if (databaseHelper != null) {

                    if (databaseHelper.getAllArticle().isEmpty()) {
                        Toast.makeText(getActivity(), "No Data", Toast.LENGTH_SHORT).show();
                        if(MyNetwork.isNetworkAvailable(getActivity())){

                            try {
                                new    GetArticlesHeadlines().execute ("http://beautifulnews.news24.com/api/article/list?cb=Beautiful-News&rankOption=3&sortOrder=1&includeArticleViews=true&pageSize=211");
                            }catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    } else {

                        articleList = databaseHelper.getAllArticle();
                        for(Article articleDB:articleList){
                            articleListWithAds.add(articleDB);
                        }
                        adapter = new RecyclerViewAdapter(getActivity(), articleListWithAds);
                        mRecyclerView.setAdapter(adapter);

                    }

                }


        }


        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh items
                refreshItems();
            }
        });

        return view;
    }
    void refreshItems() {

        if (MyNetwork.isNetworkAvailable(getActivity())) {

            try {
                new GetArticlesHeadlines().execute("http://beautifulnews.news24.com/api/article/list?cb=Beautiful-News&rankOption=3&sortOrder=1&includeArticleViews=true&pageSize=211");

            } catch (Exception e) {
                e.printStackTrace();
            }


        } else {
                    if (databaseHelper != null) {
                          articleListWithAds.clear();
                        if (databaseHelper.getArticleCount() > 0) {
                            articleList = databaseHelper.getAllArticle();
                            for (Article articleDB : articleList) {
                                articleListWithAds.add(articleDB);
                            }
                            adapter = new RecyclerViewAdapter(getActivity(), articleListWithAds);
                            mRecyclerView.setAdapter(adapter);
                            if(mSwipeRefreshLayout.isRefreshing()){
                                mSwipeRefreshLayout.setRefreshing(false);
                            }
                        }
                    }

        }
    }


    public  void readData(JSONObject jsonObjectDownloaded){
        if (jsonObjectDownloaded != null) {

            JSONArray jsonArray;

            try {
                jsonArray = jsonObjectDownloaded.getJSONArray("items");


                Article article;
                for (int count = 0; count < jsonArray.length(); count++) {
                    JSONObject jsonObjectAndroid = jsonArray.getJSONObject(count);

                    //Set JSON directly to Object
                    article = new Article(jsonObjectAndroid);

                    if (databaseHelper != null) {
                        if(databaseHelper.getArticleCount()>0) {
                            if (databaseHelper.getArticle(article.getArticleId()) != null) {
                                article.setSaved(true);
                            }
                        }
                    }

                    articleListWithAds.add(article);
                }

                if(getResources().getConfiguration().orientation== Configuration.ORIENTATION_PORTRAIT)
                addNativeExpressAds();

                adapter = new RecyclerViewAdapter(getActivity(), articleListWithAds);
                mRecyclerView.setAdapter(adapter);
                adapter.adAds(articleListWithAds);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    public void ShowSneakBar(boolean isConnected){
//if it is connected to the network close snackbar
        if(isConnected) {
            if(snackbar!=null) {
                snackbar.dismiss();
            }
            //if it is connected to the network and the RecyclerAdapter is empty download data and set
            //to prevent it to download everytime ant activity resume
        if(adapter!=null) {
            articleListWithAds.clear();
        }

                    new GetArticlesHeadlines().execute("http://beautifulnews.news24.com/api/article/list?cb=Beautiful-News&rankOption=3&sortOrder=1&includeArticleViews=true&pageSize=209");


        }else {
            //if it is not connected and was busy loading dismiss ProgressDialog


   //Write a red message on snackbar to show not connected
            String message;
            int color;

            message = "Sorry! Not connected to internet";
            color = Color.RED;
            View parentLayout = getView();

            snackbar = Snackbar
                    .make(parentLayout, message, Snackbar.LENGTH_INDEFINITE);

            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(color);
            snackbar.show();
        }
    }

    //Simple Network BroadcastReceiver to check changes on network state

    public class NetworkChangeReceiver extends BroadcastReceiver {
        public NetworkChangeReceiver() {
        }

        @Override
        public void onReceive(final Context context, final Intent intent) {

            boolean status = MyNetwork.isNetworkAvailable(context);

//if any change call this method
            ShowSneakBar(status);


        }

    }
    public void onResume() {
        super.onResume();
        //register BroadcastReceiver listener
        if (!isReceiverRegistered) {
            isReceiverRegistered = true;

           getActivity(). registerReceiver(networkChangeReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE")); // IntentFilter to wifi state change is "android.net.conn.CONNECTIVITY_CHANGE"
        }
    }
    public void onPause() {
        super.onPause();
        //unregister  BroadcastReceiver listener
        if (isReceiverRegistered) {
            isReceiverRegistered = false;

           getActivity().unregisterReceiver( networkChangeReceiver);
        }
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View views = getActivity().getLayoutInflater().inflate(R.layout.native_express_ad_container,null);

        final LinearLayout outerLayout = (LinearLayout) views.findViewById(R.id.myRoot);
        ViewTreeObserver observer = outerLayout.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @Override
            public void onGlobalLayout() {

                outerLayout.getViewTreeObserver().removeGlobalOnLayoutListener(
                        this);
            }
        });
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }


    @Override
    public void onStop() {
        super.onStop();

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().finish();

            }



    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {

//search on the list
    /* List<Article> list = new ArrayList<>();

        for (Article item :  mAdapter.getData()) {
            if (item.getTitle().toLowerCase().contains(newText.toLowerCase()) ||
                    item.getBlurb().toLowerCase().contains(newText.toLowerCase())) {
                list.add(item);

            }
        }

mAdapter.updateList(list);
*/
        return false;
    }

    private class GetArticlesHeadlines extends AsyncTask<String,Void,JSONObject> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected JSONObject doInBackground(String... params) {
            JSONParser jsonParser = new JSONParser();
            return jsonParser.getJSONFromUrl(params[0]);

        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            super.onPostExecute(jsonObject);

            if(jsonObject!=null) {


                readData(jsonObject);

                if(mSwipeRefreshLayout.isRefreshing()){
                    mSwipeRefreshLayout.setRefreshing(false);
                }

            }

        }
    }




    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception.
        try {
            mCallback = (OnHeadlineSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    /**
     * Adds Native Express ads to the items list.
     */
    private void addNativeExpressAds() {

        // Loop through the items array and place a new Native Express ad in every ith position in
        // the items List.
                View adviews = getActivity().getLayoutInflater().inflate(R.layout.native_express_ad_container,null);
                final NativeExpressAdView adView =(NativeExpressAdView) adviews.findViewById(R.id.adViewS);
                if(itemsAds.size()>1) {
            for (int i = 0; i < itemsAds.size(); i++) {
        if(!articleListWithAds.isEmpty())
                articleListWithAds.add(Integer.parseInt(itemsAds.get(i)), adView);

            }
            setUpAndLoadNativeExpressAds(adView);

        }

    }

    /**
     * Sets up and loads the Native Express ads.
     */
    private void setUpAndLoadNativeExpressAds(final NativeExpressAdView mAdView) {
        // Use a Runnable to ensure that the RecyclerView has been laid out before setting the
        // ad size for the Native Express ad. This allows us to set the Native Express ad's
        // width to match the full width of the RecyclerView.
        mRecyclerView.post(new Runnable() {
            @Override
            public void run() {


                if(itemsAds.size()>1){


                    mAdView.setAdListener(new AdListener() {
                        @Override
                        public void onAdLoaded() {
                            super.onAdLoaded();
                            // The previous Native Express ad loaded successfully, call this method again to
                            // load the next ad in the items list.

                        }

                        @Override
                        public void onAdFailedToLoad(int errorCode) {
                            // The previous Native Express ad failed to load. Call this method again to load
                            // the next ad in the items list.
                            Log.e("MainActivity", "The previous Native Express ad failed to load. Attempting to"
                                    + " load the next Native Express ad in the items list.");

                        }
                    });

                    // Load the Native Express ad.
                    AdRequest adRequest = new AdRequest.Builder()
                            //add  a test device
                            .addTestDevice("4D81EE5B524D9AA731F9F3BE231C4494")
                            .addTestDevice("42F86CCB9B15E23A97A7AD7085F0253B")
                             .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                            // .addTestDevice("4D81EE5B524D9AA731F9F3BE231C4494")
                            .build();

                    mAdView.loadAd(adRequest);
                    if(adapter!=null)
                    mRecyclerView.smoothScrollToPosition(0);

            }}
        });
    }

        public void setUpRecylceView(){
            mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
            if(!getResources().getBoolean(R.bool.portrait_only)){
                mLayoutManager = new GridLayoutManager(getContext(), 3);
            }
            mRecyclerView.setLayoutManager(mLayoutManager);

        }


}
