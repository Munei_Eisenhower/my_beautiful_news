package com.eisenhower.my_beautiful_news.Connection;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.eisenhower.my_beautiful_news.Model.Article;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;

/**
 * Created by eisenhower on 5/11/17.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    private Context context;
    private final String TAG = "DatabaseHelperClass";
    private static final int databaseVersion = 1;
    private static final String databaseName = "db_beautiful";
    private static final String TABLE_ARTICLE = "beautifulNewsArticle";

    // Image Table Columns names
    private static final String COL_ID = "col_id";
    private static final String ARTICLE_ID = "articleId";
    private static final String ARTICLE_URL = "articleUrl";
    private static final String ARTICLE_TITLE = "title";
    private static final String ARTICLE_SMALL_THUMBNAIL = "smallThumbnailUrl";
    private static final String ARTICLE_MEDIUM_THUMBNAIL = "mediumThumbnailUrl";
    private static final String ARTICLE_LARGE_THUMBNAIL = "largeThumbnailUrl";
    private static final String ARTICLE_DATE = "displayDate";
    private static final String ARTICLE_VIEWS = "articleViews";
    private static final String ARTICLE_VIDEO_URL = "articleVideoUrl";
    private static final String ARTICLE_VIDEO_FILE_PATH = "videoLocalPath";
    private static final String ARTICLE_BODY = "articleBody";
    private static final String ARTICLE_DATE_SAVED = "dateSaved";
    private static final String ARTICLE_IS_SAVED = "isSaved";

    public DatabaseHelper(Context context) {
        super(context, databaseName, null, databaseVersion);
        this.context = context;
    }
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String CREATE_ARTICLE_TABLE = "CREATE TABLE " + TABLE_ARTICLE + "("
                + COL_ID + " INTEGER PRIMARY KEY ,"
                + ARTICLE_ID + " TEXT unique,"
                + ARTICLE_TITLE + " TEXT ,"
                + ARTICLE_URL + " TEXT ,"
                + ARTICLE_BODY + " TEXT ,"
                + ARTICLE_SMALL_THUMBNAIL + " TEXT ,"
                + ARTICLE_MEDIUM_THUMBNAIL + " TEXT ,"
                + ARTICLE_LARGE_THUMBNAIL + " TEXT ,"
                + ARTICLE_DATE + " TEXT ,"
                + ARTICLE_VIEWS + " INTEGER ,"
                + ARTICLE_VIDEO_URL + " TEXT ,"
                + ARTICLE_VIDEO_FILE_PATH + " TEXT ,"
                + ARTICLE_DATE_SAVED + " TEXT ,"
                + ARTICLE_IS_SAVED + " INTEGER )";
        sqLiteDatabase.execSQL(CREATE_ARTICLE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        // Drop older table if existed
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_ARTICLE);
        onCreate(sqLiteDatabase);
    }

    public void insetArticle(Article article) {
        Log.i("jenisa",article.toString());
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(ARTICLE_ID, article.getArticleId());
        values.put(ARTICLE_URL, article.getArticleUrl());
        values.put(ARTICLE_TITLE, article.getTitle());
        values.put(ARTICLE_BODY, article.getArticleBody());
        values.put(ARTICLE_DATE, article.getDisplayDate());
        values.put(ARTICLE_VIEWS, article.getArticleViews());
        values.put(ARTICLE_VIDEO_FILE_PATH, article.getVideoLocalPath());
        values.put(ARTICLE_VIDEO_URL, article.getVideoUrl());
        values.put(ARTICLE_SMALL_THUMBNAIL, article.getSmallThumbnailUrl());
        values.put(ARTICLE_MEDIUM_THUMBNAIL, article.getMediumThumbnailUrl());
        values.put(ARTICLE_LARGE_THUMBNAIL, article.getLargeThumbnailUrl());
        values.put(ARTICLE_DATE_SAVED, article.getSavedDate());
        int isSaved = (article.isSaved())? 1 : 0;
        values.put(ARTICLE_IS_SAVED,isSaved);
        Log.i("values",values.toString());

        try {
        db.insert(TABLE_ARTICLE, null, values);
        } catch (SQLiteConstraintException e) {
            Log.d(TAG, "failure to insert word,", e);

        }
        db.close();
    }

    public Article getArticle(String id) {
        SQLiteDatabase db = this.getReadableDatabase(); Cursor cursor =null;
        Article article = null;
        try {
            cursor = db.query(TABLE_ARTICLE, new String[]{COL_ID,
                            ARTICLE_ID, ARTICLE_TITLE,ARTICLE_URL,ARTICLE_BODY,ARTICLE_SMALL_THUMBNAIL,ARTICLE_MEDIUM_THUMBNAIL,ARTICLE_LARGE_THUMBNAIL,ARTICLE_DATE,ARTICLE_VIEWS,ARTICLE_VIDEO_URL,ARTICLE_VIDEO_FILE_PATH,ARTICLE_DATE_SAVED,ARTICLE_IS_SAVED}, ARTICLE_ID + "=?",
                    new String[]{id}, null, null, null, null);

            if (cursor != null) {
                if (cursor.getCount() > 0) {
                    cursor.moveToFirst();
article = new Article();
                    article.setArticleId(cursor.getString(1));
                    article.setTitle(cursor.getString(2));
                    article.setArticleUrl(cursor.getString(3));
                    article.setArticleBody(cursor.getString(4));
                    article.setSmallThumbnailUrl(cursor.getString(5));
                    article.setMediumThumbnailUrl(cursor.getString(6));
                    article.setLargeThumbnailUrl(cursor.getString(7));
                    article.setDisplayDate(cursor.getString(8));
                    article.setArticleViews(cursor.getLong(9));
                    article.setVideoUrl(cursor.getString(10));
                    article.setVideoLocalPath(cursor.getString(11));
                    article.setSavedDate(cursor.getString(12));
                    Boolean isSaved= (cursor.getInt(13) == 1)? true : false;
                    article.setSaved(isSaved);

                }
            }
        }finally {
            if(cursor !=null)
            cursor.close();
        }

        return article;
    }



    public List<Article> getAllArticle() {
        SQLiteDatabase db = this.getWritableDatabase();
        String Query = "Select * from " + TABLE_ARTICLE ;
        Cursor cursor = db.rawQuery(Query, null);

        List<Article> articleList = new ArrayList<>();

        if (cursor.moveToFirst()) {
            do {
                Article article = new Article();
                article.setArticleId(cursor.getString(1));
                article.setTitle(cursor.getString(2));
                article.setArticleUrl(cursor.getString(3));
                article.setArticleBody(cursor.getString(4));
                article.setSmallThumbnailUrl(cursor.getString(5));
                article.setMediumThumbnailUrl(cursor.getString(6));
                article.setLargeThumbnailUrl(cursor.getString(7));
                article.setDisplayDate(cursor.getString(8));
                article.setArticleViews(cursor.getLong(9));
                article.setVideoUrl(cursor.getString(10));
                article.setVideoLocalPath(cursor.getString(11));
                article.setSavedDate(cursor.getString(12));
                Boolean isSaved= (cursor.getInt(13) == 1)? true : false;
                article.setSaved(isSaved);
Log.i("DataBaseHere",article.toString());
articleList.add(article);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return articleList;
    }

    public   boolean CheckIsDataAlreadyInDBorNot(  String fieldValue) {
        SQLiteDatabase sqldb = this.getWritableDatabase();
        String Query = "Select * from " + TABLE_ARTICLE + " where " + ARTICLE_ID+ " = '"+fieldValue+"'";
        Cursor cursor = sqldb.rawQuery(Query, null);
        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    public int getArticleCount() {

        int count=0;
        String countQuery = "SELECT  * FROM " + TABLE_ARTICLE;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        count =cursor.getCount();
        cursor.close();

        // return count
        return count;
    }
    // Deleting single contact
    public void deleteOldData() {

        if(!getReadableDatabase().isOpen()) {
            SQLiteDatabase db = this.getWritableDatabase();

            int rangeInDays = -1;

            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            Calendar range = Calendar.getInstance();
            range.add(Calendar.DAY_OF_MONTH, rangeInDays);
            HashSet<String> videoList = new HashSet<>();
            HashSet<String> videoListNew = new HashSet<>();
            for (Article article : getAllArticle()) {
                videoList.add(article.getVideoLocalPath());
            }

            int numDeleted =
                    db.delete(TABLE_ARTICLE, ARTICLE_DATE_SAVED + " < '"
                            + dateFormat.format(range.getTime()) + "'", null);

            for (Article article : getAllArticle()) {
                videoListNew.add(article.getVideoLocalPath());
            }
            for (String video : videoList) {
                if (videoListNew.add(video)) {
                    File file = new File(video);
                    if (file.exists()) {
                        file.delete();
                    }
                }
            }
            db.close();
        }

    }

    public boolean deleteArticle(String articleId) {
        SQLiteDatabase db = this.getWritableDatabase();

      int del=  db.delete(TABLE_ARTICLE, ARTICLE_ID + " = ?",
                new String[] { String.valueOf(articleId) });
        db.close();
        return del>0;
    }

}