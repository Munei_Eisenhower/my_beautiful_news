package com.eisenhower.my_beautiful_news;


import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.eisenhower.my_beautiful_news.Connection.JSONParser;
import com.eisenhower.my_beautiful_news.Model.ArticleSearch;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends Fragment implements SearchView.OnQueryTextListener {



    private RecyclerView mRecyclerView;
    private MyAdapterSearch mAdapter;

    ImageView imageView;
    SearchView searchView;
    private RecyclerView.LayoutManager mLayoutManager;

    private final long DELAY = 2000; // milliseconds
    private List<ArticleSearch> articleList = new ArrayList<>();
    public ProgressDialog mProgressDialog;


    View toCallView = null;

Handler mHandler = new Handler();
    public SearchFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bm_fragment, container, false);
        toCallView = view;
        mProgressDialog = new ProgressDialog(getContext());
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setIndeterminate(true);


        imageView = (ImageView) getActivity().findViewById(R.id.top_logo);

        searchView = (SearchView) getActivity().findViewById(R.id.tot_search);
        imageView.setVisibility(View.GONE);
        searchView.setVisibility(View.VISIBLE);
        searchView.setIconified(false);

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                imageView.setVisibility(View.VISIBLE);
                searchView.setVisibility(View.GONE);
                if(getFragmentManager()!=null){
                if (getFragmentManager().getBackStackEntryCount() > 0 ) {
                    getFragmentManager().popBackStack();
                }}

                return false;
            }
        });

        searchView.setOnQueryTextListener(this);

        setHasOptionsMenu(true);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        // mAdapter = new MyAdapter(placesList);
        mAdapter = new MyAdapterSearch(getContext());
        mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mRecyclerView.setAdapter(mAdapter);


        return view;
    }




    @Override
    public boolean onQueryTextSubmit(String query) {
        if(query.length()>2) {
            new searchArticles().execute(getResources().getString(R.string.article_search_server), query);


            searchView.clearFocus();
            mRecyclerView.requestFocus();
        }
        return true;
    }

    @Override
    public boolean onQueryTextChange(final String newText) {
        if(newText.length()>2) {
            mHandler.removeCallbacksAndMessages(null);

            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    new searchArticles().execute(getResources().getString(R.string.article_search_server), newText);

                }
            }, DELAY);
        }
        return true;
    }

    private class searchArticles extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected String doInBackground(String... params) {
            JSONParser jsonParser = new JSONParser();

            return jsonParser.POST(params[0], params[1]);

        }

        @Override
        protected void onPostExecute(String jsonObject) {
            super.onPostExecute(jsonObject);
            articleList.clear();

            try {
                if (jsonObject != null) {
                    JSONObject jsonObjectDownloaded = null;
                    try {
                        jsonObjectDownloaded = new JSONObject(jsonObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    if (jsonObjectDownloaded != null) {


                        JSONArray jsonArray;

                        try {
                            jsonArray = jsonObjectDownloaded.getJSONArray("Entities");


                            ArticleSearch articleSearch;
                            for (int count = 0; count < jsonArray.length(); count++) {
                                JSONObject jsonObjectAndroid = jsonArray.getJSONObject(count);

                                //Set JSON directly to Object
                                articleSearch = new ArticleSearch(jsonObjectAndroid);


                                articleList.add(articleSearch);

                            }



                            mAdapter.addData(articleList);


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }  catch (Exception e) {
            e.printStackTrace();
        }
        }
    }
}
