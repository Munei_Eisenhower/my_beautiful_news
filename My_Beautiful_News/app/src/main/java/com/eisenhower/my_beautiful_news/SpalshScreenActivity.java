package com.eisenhower.my_beautiful_news;

import android.app.ActivityOptions;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.PixelFormat;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Pair;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.crashlytics.android.Crashlytics;
import com.eisenhower.my_beautiful_news.Connection.JSONParser;
import com.eisenhower.my_beautiful_news.Connection.MyNetwork;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import io.fabric.sdk.android.Fabric;

public class SpalshScreenActivity extends AppCompatActivity {
    ImageView imageView_logo;
    ImageView imageView_logo_benz;
    String jsonString="";
    String myConfig="";
    private FirebaseRemoteConfig mFirebaseRemoteConfig;
    List<String> itemsAds=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        if(getResources().getBoolean(R.bool.portrait_only)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_spalsh_screen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
               .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build();
        mFirebaseRemoteConfig.setConfigSettings(configSettings);
        mFirebaseRemoteConfig.setDefaults(R.xml.remote_config_defaults);
         fetchWelcome();
        if(MyNetwork.isNetworkAvailable(this))
       new GetArticlesHeadlinesTotal().execute("http://beautifulnews.news24.com/api/article/list?cb=Beautiful-News&rankOption=3&sortOrder=1&includeArticleViews=true&pageSize=0");

        imageView_logo = (ImageView) findViewById(R.id.logo);
          imageView_logo_benz = (ImageView) findViewById(R.id.logo_benz);
        StartAnimations();
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
              openMain();

            }
        }, 3000);


    }
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        Window window = getWindow();
        window.setFormat(PixelFormat.RGBA_8888);
    }

    private void openMain(){

        Intent intent = new Intent(this,MainActivity.class);

        intent.putExtra("json",jsonString);

        intent.putExtra("adsList",myConfig);


        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            Pair[] pairs = new Pair[2];
            pairs[0]  = new Pair<View,String>(imageView_logo,"my_logo");
            pairs[1]  = new Pair<View,String>( imageView_logo_benz,"my_logo_benz");
            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(this,pairs);
            try {
                startActivity(intent, options.toBundle());
            }catch (Exception r){
                startActivity(intent);
                r.printStackTrace();

            }
            overridePendingTransition(0, 0);
        }
        else {

            startActivity(intent);
        }

    }
    private void StartAnimations() {
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.alpha);
        anim.reset();
        RelativeLayout relativeLayout=(RelativeLayout) findViewById(R.id.root);

        relativeLayout.clearAnimation();
        relativeLayout.startAnimation(anim);

        anim = AnimationUtils.loadAnimation(this, R.anim.translate);
        anim.reset();

        imageView_logo.clearAnimation();
        imageView_logo.startAnimation(anim);

        anim = AnimationUtils.loadAnimation(this, R.anim.translate2);
        anim.reset();

        imageView_logo_benz.clearAnimation();
        imageView_logo_benz.startAnimation(anim);


    }
    private void fetchWelcome() {


        long cacheExpiration = 3600; // 1 hour in seconds.
        // If your app is using developer mode, cacheExpiration is set to 0, so each fetch will
        // retrieve values from the service.
        if (mFirebaseRemoteConfig.getInfo().getConfigSettings().isDeveloperModeEnabled()) {
            cacheExpiration = 0;
        }

        // [START fetch_config_with_callback]
        // cacheExpirationSeconds is set to cacheExpiration here, indicating the next fetch request
        // will use fetch data from the Remote Config service, rather than cached parameter values,
        // if cached parameter values are more than cacheExpiration seconds old.
        // See Best Practices in the README for more information.


        // mFirebaseRemoteConfig.fetch(cacheExpiration).addOnCompleteListener()
        mFirebaseRemoteConfig.fetch(cacheExpiration)
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {

                            mFirebaseRemoteConfig.activateFetched();


                        }
                    myConfig = mFirebaseRemoteConfig.getString("advert_positions");



                    }
                });
        // [END fetch_config_with_callback]
    }

    private class GetArticlesHeadlines extends AsyncTask<String,Void,JSONObject> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected JSONObject doInBackground(String... params) {
            JSONParser jsonParser = new JSONParser();
            return jsonParser.getJSONFromUrl(params[0]);

        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            super.onPostExecute(jsonObject);

            if(jsonObject!=null) {

            jsonString = jsonObject.toString();

            }

        }
    }

    private class GetArticlesHeadlinesTotal extends AsyncTask<String,Void,JSONObject> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected JSONObject doInBackground(String... params) {
            JSONParser jsonParser = new JSONParser();
            return jsonParser.getJSONFromUrl(params[0]);

        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            super.onPostExecute(jsonObject);

            if(jsonObject!=null) {


                long count=200;
                jsonString = jsonObject.toString();
                try {
                     count = jsonObject.getLong("totalItems");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                new GetArticlesHeadlines().execute("http://beautifulnews.news24.com/api/article/list?cb=Beautiful-News&rankOption=3&sortOrder=1&includeArticleViews=true&pageSize="+count);


            }

        }
    }


}
