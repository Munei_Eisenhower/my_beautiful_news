package com.eisenhower.my_beautiful_news;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Telephony;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.eisenhower.my_beautiful_news.Connection.CheckPermission;
import com.eisenhower.my_beautiful_news.Connection.DatabaseHelper;
import com.eisenhower.my_beautiful_news.Connection.JSONParser;
import com.eisenhower.my_beautiful_news.Model.Article;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class ArticleDetailsFull extends AppCompatActivity {
    /**
     * Whether or not the system UI should be auto-hidden after
     * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
     */
    private static final boolean AUTO_HIDE = true;

    /**
     * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
     * user interaction before hiding the system UI.
     */
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

    /**
     * Some older devices needs a small delay between UI widget updates
     * and a change of the status and navigation bar.
     */
    private static final int UI_ANIMATION_DELAY = 300;
    private final Handler mHideHandler = new Handler();
    private VideoView mContentView;
    private final Runnable mHidePart2Runnable = new Runnable() {
        @SuppressLint("InlinedApi")
        @Override
        public void run() {
            // Delayed removal of status and navigation bar

            // Note that some of these constants are new as of API 16 (Jelly Bean)
            // and API 19 (KitKat). It is safe to use them, as they are inlined
            // at compile-time and do nothing on earlier devices.
            mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    };
    private View mControlsView;
    SharedPreferences preferences;
    private final Runnable mShowPart2Runnable = new Runnable() {
        @Override
        public void run() {
            // Delayed display of UI elements
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.show();
            }
            mControlsView.setVisibility(View.VISIBLE);
        }
    };
    private boolean mVisible;
    private final Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            hide();
        }
    };
    /**
     * Touch listener to use for in-layout UI controls to delay hiding the
     * system UI. This is to prevent the jarring behavior of controls going away
     * while interacting with activity UI.
     */
    private final View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (AUTO_HIDE) {
                delayedHide(AUTO_HIDE_DELAY_MILLIS);
            }
            return false;
        }
    };
    float dpHeight;
    float dpWidth;
    TextView detail_date,detail_views,detail_title,detail_body;
    DisplayImageOptions options;
    ImageLoader imageLoader;
    private final int TIMEOUT_CONNECTION = 10000;
    private final int TIMEOUT_SOCKET = 60000;
    FloatingActionButton fab;
    FloatingActionButton fab1;
    FloatingActionButton fab2;
    FloatingActionButton fab3;
    CoordinatorLayout rootLayout;
    private boolean FAB_Status = false;
    Animation show_fab_1;
    Animation hide_fab_1;
    Animation show_fab_2;
    Animation hide_fab_2;
    Animation show_fab_3;
    Animation hide_fab_3;

    //get data from last activity
    String articleId;
    String displayDate;
    String largeThumbnailUrl;
    String mediumThumbnailUrl;
    String smallThumbnailUrl;
    String blurb;
    String title;
    String articleUrl;
    String articleBody;
    long articleView=0;
    Article article;
    boolean isSaved=false;
    DatabaseHelper databaseHelper;
    ImageView imageViewThumbnail;
    boolean saveVideo=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_ACTIVITY_TRANSITIONS);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_article_details_full);

        mVisible = true;
        mControlsView = findViewById(R.id.fullscreen_content_controls);
        mContentView = (VideoView)findViewById(R.id.fullscreen_content);
        imageViewThumbnail =(ImageView)findViewById(R.id.imageViewPlay);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setIcon(R.drawable.logo);

        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        saveVideo =preferences.getBoolean("saveVideo",false);

        CheckPermission.verifyStoragePermissions(this);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab1 = (FloatingActionButton) findViewById(R.id.fab_1);
        fab2 = (FloatingActionButton) findViewById(R.id.fab_2);
        fab3 = (FloatingActionButton) findViewById(R.id.fab_3);

        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float density  = getResources().getDisplayMetrics().density;
        dpHeight = outMetrics.heightPixels / density;
        dpWidth  = outMetrics.widthPixels / density;


        detail_title = (TextView)findViewById(R.id.my_detail_text_title);
        detail_date = (TextView)findViewById(R.id.my_detail_text_date);
        detail_views = (TextView)findViewById(R.id.my_detail_text_views);
        detail_body = (TextView)findViewById(R.id.my_detail_text_body);

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.loading) // resource or drawable
                .showImageForEmptyUri(R.drawable.img_not_available) // resource or drawable
                .showImageOnFail(R.drawable.img_not_available) // resource or drawable
                .delayBeforeLoading(1000)
                .resetViewBeforeLoading(true)  // default
                .cacheInMemory(true) // default => false
                .cacheOnDisk(true) // default => false
                .build();

        imageLoader = ImageLoader.getInstance();
        databaseHelper = new DatabaseHelper(this);

        Intent intent = getIntent();
        if(intent!=null){
            Bundle bundle =intent.getExtras();
            if(bundle!=null) {
                articleId = bundle.getString("articleId", "");
                title = bundle.getString("articleTitle", "");
                displayDate = bundle.getString("articleDate", "");
                largeThumbnailUrl = bundle.getString("articleLargeThumbnailUrl", "");
                mediumThumbnailUrl = bundle.getString("articleMediumThumbnailUrl", "");
                smallThumbnailUrl = bundle.getString("articleSmallThumbnailUrl", "");
                articleUrl = bundle.getString("articleUrl", "");
                isSaved = bundle.getBoolean("isSaved", false);
            }

        }


        detail_title.setText(title);
        detail_views.setText(0 + " views");
        detail_date.setText(displayDate);

   if(isSaved){
      imageLoader.denyNetworkDownloads(true);
  }else {
       imageLoader.denyNetworkDownloads(false);
   }

  imageLoader.loadImage(smallThumbnailUrl, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {

            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                imageLoader.denyNetworkDownloads(false);
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                imageViewThumbnail.setImageBitmap(loadedImage);
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {

            }
        });

        if(isSaved){
            if(databaseHelper!=null) {
                setData(databaseHelper.getArticle(articleId));
            }else{
                new GetArticleDetails().execute(getResources().getString(R.string.article_server)+articleId);
            }
        }else{
            new GetArticleDetails().execute(getResources().getString(R.string.article_server)+articleId);
        }

       fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (FAB_Status == false) {
                    expandFAB();
                    FAB_Status = true;
                } else {
                    hideFAB();
                    FAB_Status = false;
                }
            }
        });
        fab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String shareBody = title +" \n"+articleUrl;
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, getResources().getString(R.string.share_using)));

            }
        });

        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uriUrl = Uri.parse("https://www.facebook.com/sharer.php?u="+articleUrl);
                Intent shareIntent = new Intent(Intent.ACTION_VIEW,uriUrl);
                startActivity(shareIntent);
            }
        });

        fab3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String shareBody = title +" \n"+articleUrl;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) //At least KitKat
                {
                    String defaultSmsPackageName = Telephony.Sms.getDefaultSmsPackage(ArticleDetailsFull.this); //Need to change the build to API 19

                    Intent sendIntent = new Intent(Intent.ACTION_SEND);
                    sendIntent.setType("text/plain");
                    sendIntent.putExtra(Intent.EXTRA_TEXT, shareBody );

                    if (defaultSmsPackageName != null)//Can be null in case that there is no default, then the user would be able to choose any app that support this intent.
                    {
                        sendIntent.setPackage(defaultSmsPackageName);
                    }
                    startActivity(sendIntent);

                }
                else //For early versions, do what worked for you before.
                {
                    Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                    sendIntent.setData(Uri.parse("sms:"));
                    sendIntent.putExtra("sms_body", shareBody);
                    startActivity(sendIntent);
                }

            }
        });



        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        show_fab_1 = AnimationUtils.loadAnimation(getApplication(), R.anim.fab1_show);
        hide_fab_1 = AnimationUtils.loadAnimation(getApplication(), R.anim.fab1_hide);
        show_fab_2 = AnimationUtils.loadAnimation(getApplication(), R.anim.fab2_show);
        hide_fab_2 = AnimationUtils.loadAnimation(getApplication(), R.anim.fab2_hide);
        show_fab_3 = AnimationUtils.loadAnimation(getApplication(), R.anim.fab3_show);
        hide_fab_3 = AnimationUtils.loadAnimation(getApplication(), R.anim.fab3_hide);

        // Upon interacting with UI controls, delay any scheduled hide()
        // operations to prevent the jarring behavior of controls going away
        // while interacting with the UI.


        findViewById(R.id.parent_layer).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {


if(mContentView.isShown()) {
    if (mContentView.isPlaying()) {
        mContentView.pause();

    } else {
        mContentView.start();

        findViewById(R.id.cover).setVisibility(View.GONE);
    }
    if(getResources().getBoolean(R.bool.islarge)){
        mContentView.setVisibility(View.VISIBLE);
        togglelarge();
    }else{
        toggle();
    }
}
                return false;
            }
        });




    }


    private void expandFAB() {

        //Floating Action Button 1
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) fab1.getLayoutParams();
        layoutParams.rightMargin += (int) (fab1.getWidth() * 1.7);
        layoutParams.bottomMargin += (int) (fab1.getHeight() * 0.25);
        fab1.setLayoutParams(layoutParams);
        fab1.startAnimation(show_fab_1);
        fab1.setClickable(true);

        //Floating Action Button 2
        FrameLayout.LayoutParams layoutParams2 = (FrameLayout.LayoutParams) fab2.getLayoutParams();
        layoutParams2.rightMargin += (int) (fab2.getWidth() * 1.5);
        layoutParams2.bottomMargin += (int) (fab2.getHeight() * 1.5);
        fab2.setLayoutParams(layoutParams2);
        fab2.startAnimation(show_fab_2);
        fab2.setClickable(true);

        //Floating Action Button 3
        FrameLayout.LayoutParams layoutParams3 = (FrameLayout.LayoutParams) fab3.getLayoutParams();
        layoutParams3.rightMargin += (int) (fab3.getWidth() * 0.25);
        layoutParams3.bottomMargin += (int) (fab3.getHeight() * 1.7);
        fab3.setLayoutParams(layoutParams3);
        fab3.startAnimation(show_fab_3);
        fab3.setClickable(true);
    }


    private void hideFAB() {

        //Floating Action Button 1
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) fab1.getLayoutParams();
        layoutParams.rightMargin -= (int) (fab1.getWidth() * 1.7);
        layoutParams.bottomMargin -= (int) (fab1.getHeight() * 0.25);
        fab1.setLayoutParams(layoutParams);
        fab1.startAnimation(hide_fab_1);
        fab1.setClickable(false);

        //Floating Action Button 2
        FrameLayout.LayoutParams layoutParams2 = (FrameLayout.LayoutParams) fab2.getLayoutParams();
        layoutParams2.rightMargin -= (int) (fab2.getWidth() * 1.5);
        layoutParams2.bottomMargin -= (int) (fab2.getHeight() * 1.5);
        fab2.setLayoutParams(layoutParams2);
        fab2.startAnimation(hide_fab_2);
        fab2.setClickable(false);

        //Floating Action Button 3
        FrameLayout.LayoutParams layoutParams3 = (FrameLayout.LayoutParams) fab3.getLayoutParams();
        layoutParams3.rightMargin -= (int) (fab3.getWidth() * 0.25);
        layoutParams3.bottomMargin -= (int) (fab3.getHeight() * 1.7);
        fab3.setLayoutParams(layoutParams3);
        fab3.startAnimation(hide_fab_3);
        fab3.setClickable(false);
    }



    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        delayedHide(100);
    }

    private void toggle() {
        if (mVisible) {
      hide();
            fab.setVisibility(View.INVISIBLE);

        } else {
          show();
            fab.setVisibility(View.VISIBLE);
        }
    }


    private void togglelarge() {
        if (fab.isShown()) {
            fab.setVisibility(View.INVISIBLE);
        } else {
            fab.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if(getResources().getConfiguration().orientation!= Configuration.ORIENTATION_LANDSCAPE){
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            params.addRule(RelativeLayout.CENTER_IN_PARENT);
            if(mContentView.isShown())
            mContentView.setLayoutParams(params);
        }
        else{
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            params.addRule(RelativeLayout.ALIGN_PARENT_START);
            if(mContentView.isShown())
            mContentView.setLayoutParams(params);


        }

    }

    private void hide() {
        // Hide UI first
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        mControlsView.setVisibility(View.GONE);
        mVisible = false;

       LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        findViewById(R.id.parent_layer).setLayoutParams(params1);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        if(getResources().getConfiguration().orientation!= Configuration.ORIENTATION_LANDSCAPE){
            params.addRule(RelativeLayout.CENTER_IN_PARENT);
        }
        else{
            params.addRule(RelativeLayout.ALIGN_PARENT_START);
        }


        mContentView.setLayoutParams(params);
        // Schedule a runnable to remove the status and navigation bar after a delay
        mHideHandler.removeCallbacks(mShowPart2Runnable);
        mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY);
    }

    @SuppressLint("InlinedApi")
    private void show() {
        // Show the system bar
        mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        mVisible = true;
        LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        findViewById(R.id.parent_layer).setLayoutParams(params1);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.ALIGN_PARENT_TOP);


        mContentView.setLayoutParams(params);

        // Schedule a runnable to display UI elements after a delay
        mHideHandler.removeCallbacks(mHidePart2Runnable);
        mHideHandler.postDelayed(mShowPart2Runnable, UI_ANIMATION_DELAY);
        FrameLayout.LayoutParams param = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT,FrameLayout.LayoutParams.WRAP_CONTENT);
       param.bottomMargin=getSupportActionBar().getHeight();
        param.gravity =  Gravity.BOTTOM|Gravity.RIGHT;
        fab.setLayoutParams(param);

    }

    /**
     * Schedules a call to hide() in [delay] milliseconds, canceling any
     * previously scheduled calls.
     */
    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
    //    mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if(id==android.R.id.home){

            super.onBackPressed();
        }

        else  if(id==R.id.action_save) {

            if (isSaved) {
                if (databaseHelper.deleteArticle(article.getArticleId())) {
                    Toast.makeText(getApplication(), "Article is deleted ", Toast.LENGTH_SHORT).show();

                    super.onBackPressed();
                } else {
                    Toast.makeText(getApplication(), "Article was not deleted, Try again. ", Toast.LENGTH_SHORT).show();
                }
            } else {
                new ProgressBack().execute("");

                Toast.makeText(getApplication(), "Saving Article offline. ", Toast.LENGTH_SHORT).show();
            }
        }
        return super.onOptionsItemSelected(item);
    }


    private class GetArticleDetails extends AsyncTask<String,Void,JSONObject> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            JSONParser jsonParser = new JSONParser();
            return jsonParser.getJSONFromUrl(params[0]);

        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            super.onPostExecute(jsonObject);
            if(jsonObject!=null) {
                try {
                    article = new Article(jsonObject);
                    setData(article);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }
    }

    private void setData(Article articleDetails){
        if(articleDetails!=null) {
            detail_title.setText(articleDetails.getTitle());
            detail_views.setText(articleDetails.getArticleViews() + " views");
            detail_date.setText(articleDetails.getDisplayDate());

            this.article =articleDetails;
            if(articleDetails.getArticleBody()!=null && articleDetails.getArticleBody().length()>10) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    detail_body.setText(Html.fromHtml(articleDetails.getArticleBody(), Html.FROM_HTML_MODE_COMPACT));
                } else {
                    detail_body.setText(Html.fromHtml(articleDetails.getArticleBody()));
                }
            }if(!isSaved) {
                imageLoader.loadImage(smallThumbnailUrl, new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {

                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        imageViewThumbnail.setImageBitmap(loadedImage);
                    }

                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {

                    }
                });
            }
         if(!articleDetails.getVideoUrl().equals("none")) {
             if (articleDetails.getArticleUrl() != null && articleDetails.getArticleUrl().length() > 5) {
                 if (isSaved) {
                     if(!articleDetails.getVideoLocalPath().equals("none")){
                       findViewById(R.id.imageViewPlay_icon).setVisibility(View.VISIBLE);
                        mContentView.setVisibility(View.VISIBLE);
                     mContentView.setVideoPath(articleDetails.getVideoLocalPath());
                     }
                 } else {
                     mContentView.setVisibility(View.VISIBLE);
                     findViewById(R.id.imageViewPlay_icon).setVisibility(View.VISIBLE);
                     mContentView.setVideoURI(Uri.parse(articleDetails.getVideoUrl()));

                 }

             }
         }
            else{
         findViewById(R.id.imageViewPlay_icon).setVisibility(View.GONE);
            }
        }

    }
    private class ProgressBack extends AsyncTask<String,String,String> {
        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (databaseHelper != null) {
                    Article articleToSave = article;
                    articleToSave.setSaved(true);
                    articleToSave.setVideoLocalPath(s);
                    Calendar calendar =Calendar.getInstance();
                    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                    articleToSave.setSavedDate( dateFormat.format(calendar.getTime()));

                    try {
                        databaseHelper.insetArticle(articleToSave);
                    }catch (Exception r){

                        r.printStackTrace();
                    }
                }

        }

        @Override
        protected String doInBackground(String... arg0) {
            String urlVideo = "none";
            if (saveVideo) {
                if (article.getVideoUrl().equals("none") || article.getVideoUrl() == null) {
                    return urlVideo;
                } else {
                    urlVideo = downloadFile(article.getVideoUrl(), article.getArticleId());
                    return urlVideo;
                }
            }
             return urlVideo;
        }


    }



    private String downloadFile(String fileURL, String fileName) {
        String path="none";
        try {

            String rootDir = Environment.getExternalStorageDirectory()
                    + File.separator+".savedArticles"+File.separator+fileName+".mp4" ;

            File rootFile = new File(rootDir);
            if(!rootFile.mkdir()){
                rootDir = Environment.getExternalStorageDirectory()
                        + File.separator+"savedArticles"+File.separator+fileName+".mp4" ;

            }

            URL url = new URL(fileURL );
            long startTime = System.currentTimeMillis();


            //Open a connection to that URL.
            URLConnection ucon = url.openConnection();

            //this timeout affects how long it takes for the app to realize there's a connection problem
            ucon.setReadTimeout(TIMEOUT_CONNECTION);
            ucon.setConnectTimeout(TIMEOUT_SOCKET);

            if(rootFile.exists()){
                rootFile.delete();
            }
            //Define InputStreams to read from the URLConnection.
            // uses 3KB download buffer
            InputStream is = ucon.getInputStream();
            BufferedInputStream inStream = new BufferedInputStream(is, 1024 * 5);
            FileOutputStream outStream = new FileOutputStream(rootFile);
            byte[] buff = new byte[5 * 1024];

            //Read bytes (and store them) until there is nothing more to read(-1)
            int len;
            while ((len = inStream.read(buff)) != -1) {
                outStream.write(buff, 0, len);
            }

            //clean up
            outStream.flush();
            outStream.close();
            inStream.close();
            path =rootDir;
            Log.i("Test", "download completed in for"+path+"  "
                    + ((System.currentTimeMillis() - startTime) / 1000)
                    + " sec");
        }catch (Exception e){

            e.printStackTrace();
        }
        return path;
    }

    public File getDataFolder(Context context) {
        File dataDir = null;
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            dataDir = new File(Environment.getExternalStorageDirectory(), "myappdata");
            if(!dataDir.isDirectory()) {
                dataDir.mkdirs();
            }
        }

        if(!dataDir.isDirectory()) {
            dataDir = context.getFilesDir();
        }

        return dataDir;
    }
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
if(isSaved) {
    MenuItem menuItem = menu.findItem(R.id.action_save);
    menuItem.setTitle("Delete Article");
    invalidateOptionsMenu();
}
        return super.onPrepareOptionsMenu(menu);
    }
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_detail, menu);
        return true;
    }


}
