package com.eisenhower.my_beautiful_news;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.eisenhower.my_beautiful_news.Model.Article;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by eisenhower on 5/5/17.
 */

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder>   {

    private List<Article> articles = new ArrayList<>();
    private List<Article> articlesFilrt = new ArrayList<>();

    DisplayImageOptions options;
    ImageLoader imageLoader;
    Context context;


   float alpha ;


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView title,views,date,textViewOffline;
        public ImageView coverPicture;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.my_text);
            views = (TextView) itemView.findViewById(R.id.my_text_views);
            date = (TextView) itemView.findViewById(R.id.my_text_date);
         textViewOffline = (TextView) itemView.findViewById(R.id.isSavedOffline);
            coverPicture = (ImageView) itemView.findViewById(R.id.cover_picture);


            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Article article = articles.get(getAdapterPosition());

            Intent intent = new Intent(context,ArticleDetailsFull.class);
            intent.putExtra("position",getAdapterPosition());
            intent.putExtra("articleId",article.getArticleId());
            intent.putExtra("articleTitle",article.getTitle());
            intent.putExtra("articleDate",article.getDisplayDate());
            intent.putExtra("articleSmallThumbnailUrl",article.getSmallThumbnailUrl());
            intent.putExtra("articleMediumThumbnailUrl",article.getMediumThumbnailUrl());
            intent.putExtra("articleLargeThumbnailUrl",article.getLargeThumbnailUrl());
            intent.putExtra("articleUrl",article.getArticleUrl());
            intent.putExtra("isSaved",article.isSaved());
            if(article.isSaved()) {
                intent.putExtra("articleVideoPath", article.getVideoLocalPath());
                intent.putExtra("articleVideoUrl", article.getVideoUrl());
                intent.putExtra("articleViews", article.getArticleViews());
                intent.putExtra("articleBody", article.getArticleBody());
            }

           context.startActivity(intent);

            Log.v("myPos",getAdapterPosition()+" we are here");
        }
    }



    public MyAdapter(Context context) {
this.context = context;
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.loading) // resource or drawable
                .showImageForEmptyUri(R.drawable.img_not_available) // resource or drawable
                .showImageOnFail(R.drawable.img_not_available) // resource or drawable
                .delayBeforeLoading(1000)
                .resetViewBeforeLoading(true)  // default
                .cacheInMemory(true) // default => false
                .cacheOnDisk(true) // default => false
                .build();
        imageLoader = ImageLoader.getInstance();
    }

    public void addData(List<Article> articleList){
        articles =articleList;
        articlesFilrt=articleList;
        notifyDataSetChanged();

    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        Article article =  articles.get(position);

        if(article.isSaved()){
            imageLoader.denyNetworkDownloads(true);
            holder.views.setVisibility(View.VISIBLE);
            holder.textViewOffline.setVisibility(View.VISIBLE);

        }else{
            imageLoader.denyNetworkDownloads(false);
            holder.views.setVisibility(View.INVISIBLE);
            holder.textViewOffline.setVisibility(View.INVISIBLE);
        }
        holder.title.setText(article.getTitle());

        holder.views.setText(article.getArticleViews()+" views");
        holder.date.setText(article.getDisplayDate());



        if(context. getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            imageLoader.displayImage(article.getMediumThumbnailUrl(), holder.coverPicture, options, null);
        }else{

           imageLoader.displayImage(article.getSmallThumbnailUrl(),holder.coverPicture, options, null);


        }




    }

    @Override
    public int getItemCount() {
        return articles.size();
    }




    public  List<Article> getData(){
       return  articlesFilrt;
    }

    public void updateList(List<Article> articles1){
        articles=articles1;
        notifyDataSetChanged();
    }




}