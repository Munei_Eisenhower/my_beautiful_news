package com.eisenhower.my_beautiful_news;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;

public class MyWebView extends AppCompatActivity {

    String url="";
    WebView webView ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_story);
       getSupportActionBar().hide();
        webView =(WebView)findViewById(R.id.myWebView);

        Bundle bundle = getIntent().getExtras();
        if(bundle!=null){
            url = bundle.getString("url","http://beautifulnews.news24.com");

        }
        WebSettings webSettings =  webView.getSettings();
        webSettings.setPluginState(WebSettings.PluginState.ON);
        webSettings.setJavaScriptEnabled(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setDomStorageEnabled(true);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(url);
        webView.setWebChromeClient(new WebChromeClient() {});



    }
}
