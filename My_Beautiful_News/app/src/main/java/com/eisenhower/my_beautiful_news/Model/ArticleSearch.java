package com.eisenhower.my_beautiful_news.Model;

import org.json.JSONObject;

/**
 * Created by eisenhower on 5/10/17.
 */

public class ArticleSearch {
    boolean isSaved=false;
    String articleId;
    String publishedDate;
    String displayDate;
    String thumbnailLargeURL="";
    String thumbnailMediumURL="";
    String thumbnailURL="";
    String images="";
    String author;
    String title;
    String mobileURL;
    String articleURL;
    String body;
    long totalEntities;
  long pageSize;

    String videoURL;
    long pageNo;

    JSONObject jsonObject;

    public ArticleSearch(JSONObject jsonObject) {
        this.jsonObject = jsonObject;

        try {
            readJson();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArticleSearch() {
    }

    private void readJson() throws Exception{

        if(jsonObject !=null){

            articleId = jsonObject.optString("articleId");
            articleURL = jsonObject.optString("articleURL");
            author = jsonObject.optString("author");
            body = jsonObject.optString("body");
            mobileURL = jsonObject.optString("mobileURL");
            title = jsonObject.optString("title");
            videoURL = jsonObject.optString("videoURL");
            totalEntities = jsonObject.optLong("totalEntities");
            pageNo = jsonObject.optLong("pageNo");
            pageSize = jsonObject.optLong("pageSize");
            if(jsonObject.opt("thumbnailLargeURL")!=null) {
                thumbnailLargeURL = jsonObject.optString("thumbnailLargeURL", " ");
            }
            if(jsonObject.optString("thumbnailMediumURL")!=null) {
                thumbnailMediumURL = jsonObject.optString("thumbnailMediumURL", " ");
            }
            if(jsonObject.optString("thumbnailURL")!=null) {
                thumbnailURL = jsonObject.optString("thumbnailURL", " ");
            }
            displayDate = jsonObject.optString("displayDate");
           publishedDate = jsonObject.optString("publishedDate");

            if(jsonObject.has("images")) {
                if(jsonObject.getJSONArray("images").length()>0)
                images = jsonObject.optJSONArray("images").optJSONObject(0).optString("Url", "none");

            }
           /* if(displayDate.contains("T")){

                String dtStart = displayDate;
                SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd' HH:mm ");
                try {
                    Date date = format.parse(dtStart);
                    //If time is needed
                  //  SimpleDateFormat sdf = new SimpleDateFormat("d MMMM yyyy HH:mm");
                    SimpleDateFormat sdf = new SimpleDateFormat("d MMMM yyyy");
                    displayDate= sdf.format(date);

                } catch (ParseException e) {


                    e.printStackTrace();
                }

            }*/



        }



    }

    public String getImages() {
        return images;
    }

    public boolean isSaved() {
        return isSaved;
    }

    public void setSaved(boolean saved) {
        isSaved = saved;
    }

    public String getArticleId() {
        return articleId;
    }

    public void setArticleId(String articleId) {
        this.articleId = articleId;
    }

    public String getPublishedDate() {
        return publishedDate;
    }

    public void setPublishedDate(String publishedDate) {
        this.publishedDate = publishedDate;
    }

    public String getDisplayDate() {
        return displayDate;
    }

    public void setDisplayDate(String displayDate) {
        this.displayDate = displayDate;
    }

    public String getThumbnailLargeURL() {
        return thumbnailLargeURL;
    }

    public void setThumbnailLargeURL(String thumbnailLargeURL) {
        this.thumbnailLargeURL = thumbnailLargeURL;
    }

    public String getThumbnailMediumURL() {
        return thumbnailMediumURL;
    }

    public void setThumbnailMediumURL(String thumbnailMediumURL) {
        this.thumbnailMediumURL = thumbnailMediumURL;
    }

    public String getThumbnailURL() {
        return thumbnailURL;
    }

    public void setThumbnailURL(String thumbnailURL) {
        this.thumbnailURL = thumbnailURL;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMobileURL() {
        return mobileURL;
    }

    public void setMobileURL(String mobileURL) {
        this.mobileURL = mobileURL;
    }

    public String getArticleURL() {
        return articleURL;
    }

    public void setArticleURL(String articleURL) {
        this.articleURL = articleURL;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public long getTotalEntities() {
        return totalEntities;
    }

    public void setTotalEntities(long totalEntities) {
        this.totalEntities = totalEntities;
    }

    public long getPageSize() {
        return pageSize;
    }

    public void setPageSize(long pageSize) {
        this.pageSize = pageSize;
    }

    public String getVideoURL() {
        return videoURL;
    }

    public void setVideoURL(String videoURL) {
        this.videoURL = videoURL;
    }

    public long getPageNo() {
        return pageNo;
    }

    public void setPageNo(long pageNo) {
        this.pageNo = pageNo;
    }


}
