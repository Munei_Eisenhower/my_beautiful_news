package com.eisenhower.my_beautiful_news.Model;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by eisenhower on 5/10/17.
 */

public class Article {

    long m=0,tue=0,w=0,t=0,f=0,sat=0,s=0;
    long mv=0,tuev=0,wv=0,tv=0,fv=0,satv=0,sv=0;
    boolean isSaved=false;
    String articleId;
    String categoryName;
    String categoryBreadcrumb;
    String categoryUrl;
    String displayDate;
    String largeThumbnailUrl;
    String mediumThumbnailUrl;
    String smallThumbnailUrl;
    String blurb;
    String title;
    String articleUrl;
    String articleBody;
    long articleViews;
   boolean showArticleViews;
   boolean includeShareOptions;
   boolean isPremium;
    String videoUrl;
    String savedDate="";
    String videoLocalPath="";
    JSONObject jsonObject;

    public Article(JSONObject jsonObject) {
        this.jsonObject = jsonObject;

        try {
            readJson();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Article() {
    }

    private void readJson() throws Exception{

        if(jsonObject !=null){

            articleId = jsonObject.optString("articleId");
            categoryName = jsonObject.optString("categoryName");
            categoryBreadcrumb = jsonObject.optString("categoryBreadcrumb");
            categoryUrl = jsonObject.optString("categoryUrl");

            largeThumbnailUrl = jsonObject.optString("largeThumbnailUrl");
            mediumThumbnailUrl = jsonObject.optString("mediumThumbnailUrl");
            smallThumbnailUrl = jsonObject.optString("smallThumbnailUrl");
            blurb = jsonObject.optString("blurb");
            title = jsonObject.optString("title");
            articleUrl = jsonObject.optString("articleUrl");
            articleBody = jsonObject.optString("articleBody");
            articleViews = jsonObject.optLong("articleViews");
            showArticleViews = jsonObject.optBoolean("showArticleViews");
            includeShareOptions = jsonObject.optBoolean("includeShareOptions");
            isPremium = jsonObject.optBoolean("isPremium");
            displayDate = jsonObject.optString("displayDate");
            if(displayDate.contains("T")){

                String dtStart = displayDate;
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
                try {
                    Date date = format.parse(dtStart);
                    //If time is needed
                    //  SimpleDateFormat sdf = new SimpleDateFormat("d MMMM yyyy HH:mm");
                    SimpleDateFormat sdf = new SimpleDateFormat("d MMMM yyyy");
                    displayDate= sdf.format(date);

                } catch (ParseException e) {


                    e.printStackTrace();
                }

            }
            if(jsonObject.has("galleryArticleItemModel")&& jsonObject.optJSONObject("galleryArticleItemModel")!=null) {
                videoUrl = jsonObject.optJSONObject("galleryArticleItemModel").optString("url", "none");
                videoUrl = videoUrl.replace("[[resolution]]","480");

            }else{
                videoUrl="none";
            }

        }



    }

    public String getSavedDate() {
        return savedDate;
    }

    public void setSavedDate(String savedDate) {
        this.savedDate = savedDate;
    }

    public String getVideoLocalPath() {
        return videoLocalPath;
    }

    public void setVideoLocalPath(String videoLocalPath) {
        this.videoLocalPath = videoLocalPath;
    }

    public boolean isSaved() {
        return isSaved;
    }

    public void setSaved(boolean saved) {
        isSaved = saved;
    }

    public String getArticleId() {
        return articleId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public String getCategoryBreadcrumb() {
        return categoryBreadcrumb;
    }

    public String getCategoryUrl() {
        return categoryUrl;
    }

    public String getDisplayDate() {
        return displayDate;
    }

    public String getLargeThumbnailUrl() {
        return largeThumbnailUrl;
    }

    public String getMediumThumbnailUrl() {
        return mediumThumbnailUrl;
    }

    public String getSmallThumbnailUrl() {
        return smallThumbnailUrl;
    }

    public String getBlurb() {
        return blurb;
    }

    public String getTitle() {
        return title;
    }

    public String getArticleUrl() {
        return articleUrl;
    }

    public String getArticleBody() {
        return articleBody;
    }

    public long getArticleViews() {
        return articleViews;
    }

    public boolean isShowArticleViews() {
        return showArticleViews;
    }

    public boolean isIncludeShareOptions() {
        return includeShareOptions;
    }

    public boolean isPremium() {
        return isPremium;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setArticleId(String articleId) {
        this.articleId = articleId;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public void setCategoryBreadcrumb(String categoryBreadcrumb) {
        this.categoryBreadcrumb = categoryBreadcrumb;
    }

    public void setCategoryUrl(String categoryUrl) {
        this.categoryUrl = categoryUrl;
    }

    public void setDisplayDate(String displayDate) {
        this.displayDate = displayDate;
    }

    public void setLargeThumbnailUrl(String largeThumbnailUrl) {
        this.largeThumbnailUrl = largeThumbnailUrl;
    }

    public void setMediumThumbnailUrl(String mediumThumbnailUrl) {
        this.mediumThumbnailUrl = mediumThumbnailUrl;
    }

    public void setSmallThumbnailUrl(String smallThumbnailUrl) {
        this.smallThumbnailUrl = smallThumbnailUrl;
    }

    public void setBlurb(String blurb) {
        this.blurb = blurb;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setArticleUrl(String articleUrl) {
        this.articleUrl = articleUrl;
    }

    public void setArticleBody(String articleBody) {
        this.articleBody = articleBody;
    }

    public void setArticleViews(long articleViews) {
        this.articleViews = articleViews;
    }

    public void setShowArticleViews(boolean showArticleViews) {
        this.showArticleViews = showArticleViews;
    }

    public void setIncludeShareOptions(boolean includeShareOptions) {
        this.includeShareOptions = includeShareOptions;
    }

    public void setPremium(boolean premium) {
        isPremium = premium;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public Article(String articleId, String title) {
        this.articleId = articleId;
        this.title = title;
    }

    @Override
    public String toString() {
        return "Article{" +
                "isSaved=" + isSaved +
                ", articleId='" + articleId + '\'' +
                ", displayDate='" + displayDate + '\'' +
                ", largeThumbnailUrl='" + largeThumbnailUrl + '\'' +
                ", mediumThumbnailUrl='" + mediumThumbnailUrl + '\'' +
                ", smallThumbnailUrl='" + smallThumbnailUrl + '\'' +
                ", title='" + title + '\'' +
                ", articleUrl='" + articleUrl + '\'' +
                ", articleViews=" + articleViews +
                '}';
    }
}
