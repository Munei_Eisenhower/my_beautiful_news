package com.eisenhower.my_beautiful_news;

/**
 * Created by eisenhower on 5/5/17.
 */

public class Place {

   private String name;
    private int views;
    private String date;
    private int cover;

    public Place(){

    }

    public Place(String name,int views,String date,int cover){
       this.name = name;
        this.views =views;
        this.date =date;
        this.cover =cover;

    }

    public int getCover() {
        return cover;
    }

    public void setCover(int cover) {
        this.cover = cover;
    }

    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
